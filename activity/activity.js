//add users
db.users.insertMany([
{
    firstName: "Jane",
    lastName: "Doe",
    age: 21,
    department: "HR"
},
{
    firstName: "Stephen",
    lastName: "Hawking",
    age: 76,
    department: "HR"
},{
    firstName: "Neil",
    lastName: "Armstrong",
    age: 82,
    department: "HR"
}
]);

//Finding users with letter s in their first name or d in their last name:
db.users.find({$or: [
    {firstName: {$regex: "s", $options: "$i"}},
    {lastName: {$regex: "d", $options: "$i"}}
    ]},
    {
        firstName: 1,
        lastName: 1,
        _id: 0
    }
    );

//Finding users who are from the HR department and their age is greater then or equal to 70
db.users.find({$and: [
    {department: "HR"},
    {age: {$gte:70}}
    ]}
    );

//Finding users with the letter e in their first name and has an age of less than or equal to 30
db.users.find({$and: [
    {firstName: {$regex: "e", $options: "$i"}},
    {age: {$lte:70}}
    ]}
    );